FROM alpine:latest

# Alpine package verions here: https://pkgs.alpinelinux.org/packages
RUN apk --update --no-cache add \
   git \
   nodejs \
   npm

RUN npm install -g npx
RUN git clone https://github.com/facebook/create-react-app.git /home/create-react-app

#usage: npx create-react-app my-app
